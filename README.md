public class Decrypt {
    List<Long> textList = new ArrayList<>();
    String word = "";
    public Decrypt(List<Long> digitList, long d, long n)
    {
        textList = digitList;

      //  System.out.println("Nachalo: " + textList);


        for(int i = 0; i < textList.size(); i++)
        {
            long a = textList.get(i);
            a = modular_pow(a, d, n);
            textList.set(i, a);
        }


      //  System.out.println("Decrypted: " + textList);

        for (Long digit : textList)
        {
            word += Translator.swChar(digit);
        }
    }

    private long modular_pow(long digit, long power, long modul)
    {
        long c = 1;

        for (int i = 1; i <= power; i++)
        {
            c = (c * digit) % modul;
        }

        return c;
    }

    public List<Long> getTextList()
    {
        return textList;
    }

}

public class Encrypt {

    List<Long> textList = new ArrayList<>();
    String encrText = "";
    public Encrypt(String text, long e, long n)
    {
        char [] textArr = text.toCharArray();
        for (int i = 0; i < textArr.length; i++)
        {
            textList.add(Translator.swInt(textArr[i]));
        }

      //  System.out.println("Message: " + textList);


        for(int i = 0; i < textList.size(); i++)
        {
            long a = textList.get(i);
            a = modular_pow(a, e, n);
            textList.set(i, a);
        }


      //  System.out.println("Encrypted: " + textList);

        for (Long digit: textList)
        {
            encrText += digit + " ";
        }
    }

    private long modular_pow(long digit, long power, long modul)
    {
        long c = 1;

        for (int i = 1; i <= power; i++)
        {
            c = (c * digit) % modul;
        }

        return c;
    }

    public List<Long> getTextList()
    {
        return textList;
    }

}

public class Keys {
    List<Long> simpleDigits = new ArrayList<>();
    List<Long> elementsOfE = new ArrayList<>();
    List<Long> elementsOfD = new ArrayList<>();
    String open = "";
    String close = "";
    long p;
    long q;
    long n;
    long fi;
    long e;
    long d;
    long i = 2;

    public Keys()
    {
        Random random = new Random(System.currentTimeMillis());

        try
        {
            FileInputStream fstream = new FileInputStream("e.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(fstream));
            List<String> strList = new ArrayList<>();
            String strLine;

            while((strLine = reader.readLine()) != null)
            {
                strList.add(strLine);
                simpleDigits.add(Long.parseLong(strLine));
            }

            p = Integer.parseInt(strList.get(random.nextInt(strList.size())));
         //   System.out.println("Выбираю p. \n p = " + p);
            do
            {
                q = Integer.parseInt(strList.get(random.nextInt(strList.size())));
            } while (q == p);
         //   System.out.println("Выбираю q. \n q = " + q);


        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        n = p * q;
        System.out.println("n = " + n);

        fi = (p - 1) * (q - 1);
      //  System.out.println("Вычисляю fi. \n fi = " + fi);

        for (Long digit : simpleDigits)
        {
            if (digit < fi)
            {
                if (fi % digit != 0)
                {
                    elementsOfE.add(digit);
                }
            }
        }
        e = elementsOfE.get(random.nextInt(elementsOfE.size()));
        System.out.println("e = " + e);


        while (elementsOfD.isEmpty())
        {
            long a = (i * e) % fi;
            if (a == 1)
            {
                elementsOfD.add(i);
            }
            i++;
        }
        System.out.println(elementsOfD);
        d = elementsOfD.get(random.nextInt(elementsOfD.size()));
        System.out.println("d = " + d);

        open = e + " " + n + " ";
        close = d + " " + n + " ";

        System.out.println("Выбрана пара открытых ключей: [" + open +"]");
        System.out.println("Выбрана пара закрытых ключей: [" + close +"]");
    }
}

public class Main {
    static List<Long> keysList = new ArrayList<>();
    private static Keys keys;
    static Encrypt encrypt;
    static Decrypt decrypt;


    static Keys makeKeys()
    {
        keys = new Keys();
        return keys;
    }

    static List<Long> toLongList(String str)
    {
      //  System.out.println(str);
        List<Long> list = new ArrayList<>();
        String temp = "";
        char [] charArr = str.toCharArray();
        for (int i = 0; i < charArr.length; i++)
        {
            if (charArr[i] != ' ')
            {
                temp += "" + charArr[i];
            }
            else
            {

                    list.add(Long.parseLong(temp));

                    temp = "";

            }
        }
        return list;
    }

    public static void main(String[] args) {
        try (Socket client = new Socket(InetAddress.getLocalHost(), 8030); // Вызов конструктора, принимающий параметры
// LocalHost- текущий IP, и порт
                     DataOutputStream output = new
                     DataOutputStream(client.getOutputStream());
        DataInputStream input = new
                DataInputStream(client.getInputStream());
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(System.in))
) {
            while(true)
            {
                String stringKeys = input.readUTF();

                keysList = toLongList(stringKeys);
                System.out.println(keysList);
                String str = reader.readLine();
                encrypt = new Encrypt(str, keysList.get(0), keysList.get(1));

                output.writeUTF(encrypt.encrText);
                makeKeys();
                output.writeUTF(keys.open);

                String serverMsg = input.readUTF();
                List<Long> listMsg;
                listMsg = toLongList(serverMsg);
                decrypt = new Decrypt(listMsg, keys.d, keys.n);
                System.out.println("Сервер -> " + decrypt.word);

            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}

public class Translator {

    static long swInt(char key)
    {

        switch (key)
        {
            case 'a': return 0;
            case 'b': return 1;
            case 'c': return 2;
            case 'd': return 3;
            case 'e': return 4;
            case 'f': return 5;
            case 'g': return 6;
            case 'h': return 7;
            case 'i': return 8;
            case 'j': return 9;
            case 'k': return 10;
            case 'l': return 11;
            case 'm': return 12;
            case 'n': return 13;
            case 'o': return 14;
            case 'p': return 15;
            case 'q': return 16;
            case 'r': return 17;
            case 's': return 18;
            case 't': return 19;
            case 'u': return 20;
            case 'v': return 21;
            case 'w': return 22;
            case 'x': return 23;
            case 'y': return 24;
            case 'z': return 25;
            case 'A': return 26;
            case 'B': return 27;
            case 'C': return 28;
            case 'D': return 29;
            case 'E': return 30;
            case 'F': return 31;
            case 'G': return 32;
            case 'H': return 33;
            case 'I': return 34;
            case 'J': return 35;
            case 'K': return 36;
            case 'L': return 37;
            case 'M': return 38;
            case 'N': return 39;
            case 'O': return 40;
            case 'P': return 41;
            case 'Q': return 42;
            case 'R': return 43;
            case 'S': return 44;
            case 'T': return 45;
            case 'U': return 46;
            case 'V': return 47;
            case 'W': return 48;
            case 'X': return 49;
            case 'Y': return 50;
            case 'Z': return 51;
            case '0': return 52;
            case '1': return 53;
            case '2': return 54;
            case '3': return 55;
            case '4': return 56;
            case '5': return 57;
            case '6': return 58;
            case '7': return 59;
            case '8': return 60;
            case '9': return 61;
            case ' ': return 62;
            case ',': return 63;
            case '.': return 64;
            case ':': return 65;
            case ';': return 66;
            case '!': return 67;
            case '?': return 68;
            case '\"': return 69;
            case '\'': return 70;
            default:
                System.err.print("!!");
                System.exit(2);
                return -1;
        }
    }

    static char swChar(long key)
    {

        switch ((int)key)
        {
            case 0: return 'a';
            case 1: return 'b';
            case 2: return 'c';
            case 3: return 'd';
            case 4: return 'e';
            case 5: return 'f';
            case 6: return 'g';
            case 7: return 'h';
            case 8: return 'i';
            case 9: return 'j';
            case 10: return 'k';
            case 11: return 'l';
            case 12: return 'm';
            case 13: return 'n';
            case 14: return 'o';
            case 15: return 'p';
            case 16: return 'q';
            case 17: return 'r';
            case 18: return 's';
            case 19: return 't';
            case 20: return 'u';
            case 21: return 'v';
            case 22: return 'w';
            case 23: return 'x';
            case 24: return 'y';
            case 25: return 'z';
            case 26: return 'A';
            case 27: return 'B';
            case 28: return 'C';
            case 29: return 'D';
            case 30: return 'E';
            case 31: return 'F';
            case 32: return 'G';
            case 33: return 'H';
            case 34: return 'I';
            case 35: return 'J';
            case 36: return 'K';
            case 37: return 'L';
            case 38: return 'M';
            case 39: return 'N';
            case 40: return 'O';
            case 41: return 'P';
            case 42: return 'Q';
            case 43: return 'R';
            case 44: return 'S';
            case 45: return 'T';
            case 46: return 'U';
            case 47: return 'V';
            case 48: return 'W';
            case 49: return 'X';
            case 50: return 'Y';
            case 51: return 'Z';
            case 52: return '0';
            case 53: return '1';
            case 54: return '2';
            case 55: return '3';
            case 56: return '4';
            case 57: return '5';
            case 58: return '6';
            case 59: return '7';
            case 60: return '8';
            case 61: return '9';
            case 62: return ' ';
            case 63: return ',';
            case 64: return '.';
            case 65: return ':';
            case 66: return ';';
            case 67: return '!';
            case 68: return '?';
            case 69: return '\"';
            case 70: return '\'';

            default:
                System.err.print("!!");
                System.exit(3);
                return '-';
        }
    }

}